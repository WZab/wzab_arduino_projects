// Section below is taken from stm32duino ADC example

#include <stm32yyxx_ll_adc.h>
/* Values available in datasheet */
#define CALX_TEMP 25
#if defined(STM32F1xx)
#define V25 1430
#define AVG_SLOPE 4300
#define VREFINT 1200
#elif defined(STM32F2xx) || defined(STM32F4xx)
#define V25 760
#define AVG_SLOPE 2500
#define VREFINT 1210
#endif


// End of the ADC section

#include <Wire.h>

#include <BMx280I2C.h>
BMx280I2C bmx280(0X76);

#include <STM32LowPower.h>

// Serial appears on PA9/PA10
#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>

#include <Wire.h>

#include <CayenneLPP.h>
#include "credentials.h"

void os_getArtEui(u1_t* buf) {
  memcpy_P(buf, APPEUI, 8);
}
void os_getDevEui(u1_t* buf) {
  memcpy_P(buf, DEVEUI, 8);
}
void os_getDevKey(u1_t* buf) {
  memcpy_P(buf, APPKEY, 16);
}

CayenneLPP lpp(51);

static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 60;

// Pin mapping
const lmic_pinmap lmic_pins = {
  .nss = PA4,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = PC14,
  .dio = { PA2, PA3, LMIC_UNUSED_PIN },
};

/* Analog read resolution */
#define LL_ADC_RESOLUTION LL_ADC_RESOLUTION_12B
#define ADC_RANGE 4096

static float readVref() {
  return ((float)VREFINT / 1000.0 * ADC_RANGE / (float)analogRead(AVREF));  // ADC sample to mV
}

static float_t readVoltage(float VRef, uint32_t pin) {
  return VRef / ADC_RANGE * analogRead(pin);
}

void printHex2(unsigned v) {
  v &= 0xff;
  if (v < 16)
    Serial.print('0');
  Serial.print(v, HEX);
}

void onEvent(ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch (ev) {
    case EV_SCAN_TIMEOUT:
      Serial.println(F("EV_SCAN_TIMEOUT"));
      break;
    case EV_BEACON_FOUND:
      Serial.println(F("EV_BEACON_FOUND"));
      break;
    case EV_BEACON_MISSED:
      Serial.println(F("EV_BEACON_MISSED"));
      break;
    case EV_BEACON_TRACKED:
      Serial.println(F("EV_BEACON_TRACKED"));
      break;
    case EV_JOINING:
      Serial.println(F("EV_JOINING"));
      break;
    case EV_JOINED:
      Serial.println(F("EV_JOINED"));
      {
        u4_t netid = 0;
        devaddr_t devaddr = 0;
        u1_t nwkKey[16];
        u1_t artKey[16];
        LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
        Serial.print("netid: ");
        Serial.println(netid, DEC);
        Serial.print("devaddr: ");
        Serial.println(devaddr, HEX);
        Serial.print("AppSKey: ");
        for (size_t i = 0; i < sizeof(artKey); ++i) {
          if (i != 0)
            Serial.print("-");
          printHex2(artKey[i]);
        }
        Serial.println("");
        Serial.print("NwkSKey: ");
        for (size_t i = 0; i < sizeof(nwkKey); ++i) {
          if (i != 0)
            Serial.print("-");
          printHex2(nwkKey[i]);
        }
        Serial.println();
      }
      // Disable link check validation (automatically enabled
      // during join, but because slow data rates change max TX
      // size, we don't use it in this example.
      LMIC_setLinkCheckMode(0);
      break;
    /*
      || This event is defined but not used in the code. No
      || point in wasting codespace on it.
      ||
      || case EV_RFU1:
      ||     Serial.println(F("EV_RFU1"));
      ||     break;
    */
    case EV_JOIN_FAILED:
      Serial.println(F("EV_JOIN_FAILED"));
      break;
    case EV_REJOIN_FAILED:
      Serial.println(F("EV_REJOIN_FAILED"));
      break;
    case EV_TXCOMPLETE:
      Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
      if (LMIC.txrxFlags & TXRX_ACK)
        Serial.println(F("Received ack"));
      if (LMIC.dataLen) {
        Serial.print(F("Received "));
        Serial.print(LMIC.dataLen);
        Serial.println(F(" bytes of payload"));
      }
      // Schedule next transmission
      do_send(&sendjob);
      for (int i = 0; i < int(TX_INTERVAL / 8); i++) {
        // Use library from https://github.com/rocketscream/Low-Power
        LowPower.deepSleep(8000);
      }
      break;
    case EV_LOST_TSYNC:
      Serial.println(F("EV_LOST_TSYNC"));
      break;
    case EV_RESET:
      Serial.println(F("EV_RESET"));
      break;
    case EV_RXCOMPLETE:
      // data received in ping slot
      Serial.println(F("EV_RXCOMPLETE"));
      break;
    case EV_LINK_DEAD:
      Serial.println(F("EV_LINK_DEAD"));
      break;
    case EV_LINK_ALIVE:
      Serial.println(F("EV_LINK_ALIVE"));
      break;
    /*
      || This event is defined but not used in the code. No
      || point in wasting codespace on it.
      ||
      || case EV_SCAN_FOUND:
      ||    Serial.println(F("EV_SCAN_FOUND"));
      ||    break;
    */
    case EV_TXSTART:
      Serial.println(F("EV_TXSTART"));
      break;
    case EV_TXCANCELED:
      Serial.println(F("EV_TXCANCELED"));
      break;
    case EV_RXSTART:
      /* do not print anything -- it wrecks timing */
      break;
    case EV_JOIN_TXCOMPLETE:
      Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
      break;

    default:
      Serial.print(F("Unknown event: "));
      Serial.println((unsigned)ev);
      break;
  }
}

void do_send(osjob_t* j) {
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {
    Serial.println(F("OP_TXRXPEND, not sending"));
  } else {
    float t, p, h, v;
    // Switch on the voltage divider
    digitalWrite(PA0, HIGH);
    //climateSensor.takeForcedMeasurement();
    while (!bmx280.measure()) {};
    do {
      delay(100);
    } while (!bmx280.hasValue());
    t = bmx280.getTemperature();
    p = bmx280.getPressure() / 100.0;
    h = bmx280.getHumidity();
    delay(200);
    Serial.print(F(" Pressure: "));
    Serial.print(p);
    Serial.print(F(" Temperature: "));
    Serial.print(t);
    Serial.print(F(" Humidity: "));
    Serial.print(h);
    // Measure the voltage
    float VRef = readVref();
    Serial.print(F(" Voltage Ref: "));
    Serial.print(VRef);
    v = 2.0 * readVoltage(VRef, PA1);  //The voltage is measured after the voltage divider 100k+100k
    // Switch off the voltage divider
    digitalWrite(PA0, LOW);
    Serial.print(F(" Voltage: "));
    Serial.print(v);
    lpp.reset();
    lpp.addTemperature(1, t);
    lpp.addBarometricPressure(2, p);
    lpp.addRelativeHumidity(3, h);
    lpp.addAnalogInput(4, v);  //When I tried to add the voltage here TTN was not able to unpack the payload
                               //Therefore I switched to AnalogInput, but now Cayenne refuses to include it into the project data...
    lpp.addGPS(5, 52.1291065, 21.0638952, 130.0);
    Serial.println();

    // Prepare upstream data transmission at the next possible time.
    LMIC_setTxData2(1, lpp.getBuffer(), lpp.getSize(), 0);
    Serial.println(F("Packet queued"));
    Serial.flush();
  }
  // Next TX is scheduled after TX_COMPLETE event.
}

void setup() {
  Serial.begin(115200);
  Serial.println(F("Starting"));
  analogReadResolution(12);
  pinMode(PA0, OUTPUT);
  digitalWrite(PA0, LOW);
  pinMode(PA1, INPUT_ANALOG);
  pinMode(AVREF, INPUT_ANALOG);
  LowPower.begin();
  Wire.setSDA(PB11);
  Wire.setSCL(PB10);
  Wire.begin();
  if (!bmx280.begin()) {
    Serial.println("begin() failed. check your BMx280 Interface and I2C Address.");
    while (1)
      ;
  }

  if (bmx280.isBME280())
    Serial.println("sensor is a BME280");
  else
    Serial.println("sensor is a BMP280");

  //reset sensor to default parameters.
  bmx280.resetToDefaults();
  //by default sensing is disabled and must be enabled by setting a non-zero
  //oversampling setting.
  //set an oversampling setting for pressure and temperature measurements.
  bmx280.writeOversamplingPressure(BMx280MI::OSRS_P_x16);
  bmx280.writeOversamplingTemperature(BMx280MI::OSRS_T_x16);

  //if sensor is a BME280, set an oversampling setting for humidity measurements.
  if (bmx280.isBME280())
    bmx280.writeOversamplingHumidity(BMx280MI::OSRS_H_x16);
  //WZab: It looks like this setting must be done at the end...
  bmx280.writePowerMode(BMx280MI::BMx280_MODE_FORCED);
  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

  // Start job (sending automatically starts OTAA too)
  do_send(&sendjob);
}

void loop() {
  os_runloop_once();
}