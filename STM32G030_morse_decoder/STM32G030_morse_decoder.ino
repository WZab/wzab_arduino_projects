#include "MorseDecoder.h";

boolean leftKey = false;
boolean rightKey = false;
boolean lastWasKey = true;
boolean speedChanged = false;

HardwareSerial Serial1(PB_7,PB_6);
void displayMorse(String symbol, boolean decoded)
{
  Serial1.print(symbol);
}
Decoder dec = Decoder(true);
void setup() {
  // put your setup code here, to run once:
  Serial1.begin(115200);
  Serial1.println("Morse decoder 0.2");
  dec.setup();
}

void loop() {
  // put your main code here, to run repeatedly:
  dec.decode();
}
