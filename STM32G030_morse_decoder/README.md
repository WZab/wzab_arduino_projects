This Morse code decoder is taken from [Morserino 32 project](https://github.com/oe1wkl/Morserino-32) by Willi Kraml.
I (Wojciech Zabolotny) have removed functions related to audio decoding. Only the straight key is supported.
The decoded characters are sent via serial output (but it can be easily changed).
Now the design fits and works correctly even in [STM32G030F6P6 board from WeAct](https://github.com/WeActStudio/WeActStudio.STM32G0xxC0xxCoreBoard). However, it occupies 90% of the code space.

